Pasta para os conjuntos de dados utilizados. Tais conjuntos são:

- `doentes.csv`: Dados fictícios de doentes em duas cidades goianas. Criação própria.
- `filmes_terror.csv`: Web scraping da base do IMDb especificamente para a categoria de terror (*horror*). Compilado em 06/2020 (compilação própria).
- `multas.csv`: Dados fictícios de multas de trânsito. Criação própria.
- `bmi.csv`: Dados de gênero, peso, altura e idade (adaptado do [kaggle](https://www.kaggle.com/datasets/yasserh/bmidataset)).
- `books.csv`: Dados de vários livros retirados do site Goodreads (adaptado do [kaggle](https://www.kaggle.com/datasets/jealousleopard/goodreadsbooks)).
- `abundancia.csv`: Dados fictícios da abundância de 20 espécies hipotéticas em 400 locais diferentes. Criação própria.
- `insurance.csv`: Dados de custo de seguro médico (retirado do [kaggle](https://www.kaggle.com/datasets/mirichoi0218/insurance)).
